TARGET=infos_rentree_conscrits
all: $(TARGET).pdf $(TARGET).en.pdf

%.pdf: %.tex
	latexmk -xelatex -pdf $<

clean:
	rm -f *aux *bbl *bcf *blg *_latexmk *fls *log *out *.run.xml
