# Infos rentrée conscrits

Une feuille A4 recto/verso max, distribuée aux conscrit·e·s à la rentrée. Détaille les services informatiques élèves à l'ENS, ainsi que quelques infos plus générales sur l'informatique à l'ENS (pas nécessairement élèves).